/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "TankController.h"
#include <Config.h>
#include "Entity.h"
#include "EntityFactory.h"
#include "EntityRepresentationFactory.h"
#include "EntityManager.h"
#include "EntityRepresentationManager.h"
#include "EntityRepresentation.h"

#include <iostream>

namespace SpaceInvaders {

  TankController::TankController(int leftBorder, int rightBorder,
      EntityFactory *entityFactory,
      EntityRepresentationFactory *representationFactory)
    : m_leftBorder(leftBorder), m_rightBorder(rightBorder),
      m_entityFactory(entityFactory),
      m_representationFactory(representationFactory)
  {
  }

  TankController::~TankController()
  {
  }

  Entity* TankController::tank() const
  {
    if (auto ptr = m_tank.lock())
      return ptr.get();
    return 0;
  }

  void TankController::setTank(const std::shared_ptr<Entity> &tank)
  {
    m_tank = tank;
  }

  void TankController::left(float x)
  {
    auto ptr = m_tank.lock();
    if (!ptr)
      return;

    ptr->move(-x, 0);
    if (ptr->position().x < m_leftBorder)
      ptr->setPosition(m_leftBorder, ptr->position().y);
  }

  void TankController::right(float x)
  {
    auto ptr = m_tank.lock();
    if (!ptr)
      return;

    ptr->move(x, 0);
    if (ptr->position().x > m_rightBorder)
      ptr->setPosition(m_rightBorder, ptr->position().y);
  }

  void TankController::fire()
  {
    auto ptr = m_tank.lock();
    if (!ptr)
      return;

    double elapsed = m_clock.getElapsedTime().asSeconds();

    if (elapsed < 1.0)
      return;
    m_clock.restart();

    std::shared_ptr<Entity> bullet = m_entityFactory->createBullet(-1);
    auto tankPos = ptr->position();
    bullet->setPosition(tankPos.x + PIXEL_SIZE * 5, tankPos.y - PIXEL_SIZE * 3);
    EntityManager::instance()->addEntity(bullet);
    std::shared_ptr<EntityRepresentation> repr = m_representationFactory->createBulletRepresentation(bullet);
    bullet->addObserver(repr);
    EntityRepresentationManager::instance()->addRepresentation(repr);
  }

}
