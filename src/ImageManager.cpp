/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "ImageManager.h"
#include <Config.h>
#include "impl/string.h"

#include <cassert>
#include <stdexcept>

namespace SpaceInvaders {

  ImageManager::ImageManager() : m_directory(".")
  {
  }

  ImageManager* ImageManager::instance()
  {
    static ImageManager *obj = 0;

    if (!obj)
      obj = new ImageManager;

    return obj;
  }

  sf::Image& ImageManager::image(const std::string &filename)
  {
    auto img = m_images.find(filename);

    // try to load the image if it is not in the map
    if (img == m_images.end())
      loadImage(filename);

    return img->second;
  }

  void ImageManager::loadImage(const std::string &filename)
  {
    if (!m_images[filename].loadFromFile(m_directory + filename))
      throw std::runtime_error(make_string("Could not load image ", m_directory, "/", filename));
  }

  sf::Image& ImageManager::create(const std::string &filename, int width, int height)
  {
    // add new empty image to the map
    m_images[filename] = sf::Image();
    sf::Image &img = m_images[filename];
    // create the image with specfified dimensions
    img.create(width, height);
    return img;
  }

}
