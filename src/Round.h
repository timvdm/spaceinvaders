/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SPACEINVADERS_ROUND_H
#define SPACEINVADERS_ROUND_H

#include <SFML/Graphics.hpp>
#include <memory>

namespace SpaceInvaders {

  /**
   * @file Round.h
   */

  // fwd declarations
  class EntityFactory;
  class EntityRepresentationFactory;
  class InvaderController;
  class TankController;
  class ScoreCounter;
  class LifeCounter;
  class Overlay;

  /**
   * @brief Class for playing a single round.
   */
  class Round
  {
    public:
      /**
       * @brief Constructor.
       *
       * @param entityFactory The entity factory.
       * @param representationFactory The entity representation factory.
       * @param invaderController The controller for the invaders.
       * @param tankController The controller for the tank.
       */
      Round(EntityFactory *entityFactory, EntityRepresentationFactory *representationFactory,
          InvaderController *invaderController, TankController *tankController,
          std::shared_ptr<ScoreCounter> &scoreCounter, std::shared_ptr<LifeCounter> &lifeCounter);

      /**
       * @brief Initialize the round.
       */
      void init();

      /**
       * @brief Update game logic and representations.
       *
       * @param window The window to render to.
       */
      void update(sf::RenderWindow &window);

    private:
      /**
       * @brief Create a row of invaders.
       *
       * @param type The type of invader (i.e. 1, 2, 3).
       * @param yOffset Y offset where to place the invaders.
       * @param score The score associated with this row.
       */
      void createRow(int type, int yOffset, int score);

      /**
       * @brief Create a single barricade block.
       *
       * @param type The type of block.
       * @param x The x offset in block coordinates.
       * @param y The y offset in block coordinates.
       * @param offsetX The x offset on the screen.
       * @param offsetY The y offset on the screen.
       */
      void createBarricadeBlock(int type, int x, int y, int offsetX, int offsetY);

      /**
       * @brief Create a barricade.
       *
       * @param x The x screen position.
       * @param y The y screen position.
       */
      void createBarricade(int x, int y);

      /**
       * @brief Draw bounding boxes for all entities.
       *
       * This function is used for debugging.
       */
      void drawBoundingBoxes(sf::RenderWindow &window);

      /**
       * @brief Check if two bounding boxes collide.
       *
       * @param pos1 Upper-left corner of bounding box 1.
       * @param bb1 Width and height of bounding box 1.
       * @param pos2 Upper-left corner of bounding box 2.
       * @param bb2 Width and height of bounding box 2.
       *
       * @return True if the bounding boxes collide.
       */
      bool collides(const sf::Vector2f &pos1, const sf::Vector2f &bb1,
          const sf::Vector2f &pos2, const sf::Vector2f &bb2);

      /**
       * @brief Perform collision detection.
       */
      void collisionDetection();

      sf::Clock m_clock; //!< The clock for timing updates.
      EntityFactory *m_entityFactory; //!< The entity factory.
      EntityRepresentationFactory *m_representationFactory; //!< The representation factory.
      InvaderController *m_invaderController; //!< The invader controller.
      TankController *m_tankController; //!< The tank controller.
      std::shared_ptr<ScoreCounter> m_scoreCounter; //!< The score counter.
      std::shared_ptr<LifeCounter> m_lifeCounter; //!< The life counter.
      std::shared_ptr<Overlay> m_overlay; //!< The overlay representation.
  };

}

#endif
