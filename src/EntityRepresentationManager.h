/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SPACEINVADERS_ENTITYREPRESENTATIONMANAGER_H
#define SPACEINVADERS_ENTITYREPRESENTATIONMANAGER_H

#include <vector>
#include <memory>

namespace sf {
  class RenderWindow;
}

namespace SpaceInvaders {

  /**
   * @file EntityRepresentationManager.h
   */

  // forward declarations
  class Entity;
  class EntityRepresentation;

  /**
   * @brief Class for managing entity representations.
   *
   * All entity representations in the game need to be added to the
   * representation manager. The representation manager will call the
   * EntityRepresentation::draw() function for all entity representations and
   * performs other bookkeeping tasks. All manager classes are singletons (with
   * private constructors) to ensure only one manager of each type exists. To
   * obtain a pointer to the instance, the static
   * EntityRepresentationManager::instance() can be used:
   *
   @code
   EntityRepresentationManager *representationManager = EntityRepresentationManager::instance();
   @endcode
   *
   * This pointer must never be deleted.
   *
   * Entity representations are added to the manager using
   * EntityRepresentationManager::addEntity() and can be removed indirectly by calling
   * EntityRepresentation::removeLater(). The EntityRepresentationManager does not
   * remove the entity representation directly. Instead, at the end of an update,
   * the EntityRepresentationManager::cleanup() function removes all entity
   * representations that were scheduled to be removed during the update. This
   * ensures a consistent view of entity representations during an update.
   *
   * A typical usage of the manager classes is given in the example below:
   @code
   sf::Clock clock;
   while (true) {
     // get elapsed time
     sf::Time elapsed = clock.restart();

     // call draw
     EntityRepresentationManager::instance()->draw(window);

     // perform other tasks such as collision detection, game logic, ...

     // call manager cleanup fucntions
     EntityRepresentationManager::instance()->cleanup();
   }
   @endcode
   *
   * All entities and entity representations use shared pointers to make memory
   * management easier and safer. For example, and entity might be hit and removed
   * while the representation lives slightly longer to show an explosion animation.
   * The use of shared pointers ensures the Entity can still be accessed by the
   * EntityRepresentation after it has been removed ("deleted") from the entity
   * manager.
   */
  class EntityRepresentationManager
  {
    public:
      /**
       * @brief Get the singleton representation manager instance.
       *
       * @return The representation manager.
       */
      static EntityRepresentationManager* instance();

      /**
       * @brief Add an entity representation.
       *
       * @param representation The entity representation to add.
       */
      void addRepresentation(const std::shared_ptr<EntityRepresentation> &representation);

      /**
       * @brief Get the entity representations.
       *
       * @return The entity representations.
       */
      const std::vector<std::shared_ptr<EntityRepresentation>>& representations() const
      {
        return m_representations;
      }

      /**
       * @brief Draw all entity representations.
       *
       * @param window The SFML render window.
       */
      void draw(sf::RenderWindow &window);

      /**
       * @brief The cleanup function to be called at the end of an update.
       *
       * This function removes the entity representations that were scheduled
       * to be removed during the update.
       */
      void cleanup();

    private:
      // needed for removeLater() below
      friend class EntityRepresentation;

      /**
       * @brief Schedule an entity representation to be removed.
       *
       * @param representation The entity representation.
       */
      void removeLater(EntityRepresentation *representation);

      /**
       * @brief Constructor.
       *
       * This constructor is private to ensure only a single instance can exist.
       * To obtain a pointer to the singleton instance, use the static
       * EntityRepresentationManager::instance() function.
       */
      EntityRepresentationManager();

      std::vector<std::shared_ptr<EntityRepresentation>> m_representations; //!< The entity representations.
      std::vector<EntityRepresentation*> m_removedRepresentations; // !< Entity representations to be removed.
  };

}

#endif
