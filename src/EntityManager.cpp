/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "EntityManager.h"
#include "Entity.h"
#include "impl/util.h"

#include <cassert>

namespace SpaceInvaders {

  EntityManager::EntityManager()
  {
  }

  EntityManager* EntityManager::instance()
  {
    // singleton instance
    static EntityManager *obj = 0;

    // create the instance if it does not exist
    if (!obj)
      obj = new EntityManager;

    return obj;
  }

  void EntityManager::addEntity(const std::shared_ptr<Entity> &entity)
  {
    m_entities.push_back(entity);
  }

  void EntityManager::removeLater(Entity *entity)
  {
    m_removedEntities.push_back(entity);
  }

  void EntityManager::cleanup()
  {
    // remove all entities that called removeLater() in the update
    for (auto entity : m_removedEntities) {
      auto iter = std::find_if(m_entities.begin(), m_entities.end(), CompareSharedPtr<Entity>(entity));
      if (iter != m_entities.end())
        m_entities.erase(iter);
    }
    m_removedEntities.clear();
  }

  void EntityManager::update(double elapsed)
  {
    // update all entities
    for (auto entity : m_entities) {
      assert(entity.get());
      entity->update(elapsed);
    }
  }

}
