#ifndef SPACEINVADERS_INVADER_H
#define SPACEINVADERS_INVADER_H

#include "../Entity.h"
#include <Config.h>
#include "../Notification.h"

namespace SpaceInvaders {

  class Invader : public Entity
  {
    public:
      Invader(int score) : m_score(score)
      {
      }

      virtual ~Invader()
      {
      }

      sf::Vector2f boundingBox() const
      {
        return sf::Vector2f(12 * PIXEL_SIZE, 8 * PIXEL_SIZE);
      }

      void update(double time)
      {
      }

      void hit(int damage)
      {
        if (damage) {
          removeLater();
          notifyObservers(NotifyScore + m_score);
        }
      }

    private:
      int m_score;
  };

}

#endif
