#ifndef SPACEINVADERS_DEFAULTFACTORY_H
#define SPACEINVADERS_DEFAULTFACTORY_H

#include "../EntityFactory.h"

namespace SpaceInvaders {

  class DefaultEntityFactory : public EntityFactory
  {
    public:
      std::shared_ptr<Entity> createInvader(int score);
      std::shared_ptr<Entity> createTank();
      std::shared_ptr<Entity> createBullet(int dir);
      std::shared_ptr<Entity> createBarricade();
  };

}

#endif
