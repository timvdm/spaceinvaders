#include "Bullet.h"

namespace SpaceInvaders {

  void Bullet::update(double elapsed)
  {
    move(0, m_dir * 200.0 * elapsed);
  }

}
