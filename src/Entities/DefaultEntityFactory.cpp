#include "DefaultEntityFactory.h"
#include "Invader.h"
#include "Tank.h"
#include "Bullet.h"
#include "Barricade.h"

namespace SpaceInvaders {

  std::shared_ptr<Entity> DefaultEntityFactory::createInvader(int score)
  {
    return std::shared_ptr<Entity>(new Invader(score));
  }

  std::shared_ptr<Entity> DefaultEntityFactory::createTank()
  {
    return std::shared_ptr<Entity>(new Tank);
  }

  std::shared_ptr<Entity> DefaultEntityFactory::createBullet(int dir)
  {
    return std::shared_ptr<Entity>(new Bullet(dir));
  }

  std::shared_ptr<Entity> DefaultEntityFactory::createBarricade()
  {
    return std::shared_ptr<Entity>(new Barricade());
  }

}
