#ifndef SPACEINVADERS_BARRICADE_H
#define SPACEINVADERS_BARRICADE_H

#include "../Entity.h"
#include <Config.h>
#include "../Notification.h"

namespace SpaceInvaders {

  class Barricade : public Entity
  {
    public:
      Barricade() : m_health(4)
      {
      }

      virtual ~Barricade()
      {
      }

      sf::Vector2f boundingBox() const
      {
        return sf::Vector2f(6 * PIXEL_SIZE, 6 * PIXEL_SIZE);
      }

      void update(double time)
      {
      }

      void hit(int damage)
      {
        m_health -= damage;

        if (!m_health)
          removeLater();

        notifyObservers(NotifyHit);
      }

    private:
      int m_health;
  };

}

#endif
