#ifndef SPACEINVADERS_BULLET_H
#define SPACEINVADERS_BULLET_H

#include "../Entity.h"
#include <Config.h>

namespace SpaceInvaders {

  class Bullet : public Entity
  {
    public:
      Bullet(int dir) : m_dir(dir)
      {
      }

      virtual ~Bullet()
      {
      }

      sf::Vector2f boundingBox() const
      {
        return sf::Vector2f(PIXEL_SIZE, 3 * PIXEL_SIZE);
      }

      void update(double time);

      void hit(int damage)
      {
        removeLater();
      }

      int damage() const
      {
        return 1;
      }

    private:
      int m_dir;
  };

}

#endif
