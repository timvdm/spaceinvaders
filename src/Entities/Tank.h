#ifndef SPACEINVADERS_TANK_H
#define SPACEINVADERS_TANK_H

#include "../Entity.h"

namespace SpaceInvaders {

  class Tank : public Entity
  {
    public:
      Tank()
      {
      }

      virtual ~Tank()
      {
      }

      sf::Vector2f boundingBox() const
      {
        return sf::Vector2f(13 * PIXEL_SIZE, 7 * PIXEL_SIZE);
      }

      void update(double time)
      {
      }

      void hit(int damage)
      {
        notifyObservers(NotifyHit);
      }
  };

}

#endif
