/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SPACEINVADERS_ENTITYFACTORY_H
#define SPACEINVADERS_ENTITYFACTORY_H

#include <memory>

namespace SpaceInvaders {

  /**
   * @file EntityFactory.h
   */

  // forward declaration
  class Entity;

  /**
   * @brief Base class for entity factories.
   */
  class EntityFactory
  {
    public:
      /**
       * @brief Destructor.
       */
      virtual ~EntityFactory();

      /**
       * @brief Create invader entity.
       *
       * @return The created invader entity.
       */
      virtual std::shared_ptr<Entity> createInvader(int score) = 0;

      /**
       * @brief Create tank entity.
       *
       * @return The created tank entity.
       */
      virtual std::shared_ptr<Entity> createTank() = 0;

      /**
       * @brief Create bullet entity.
       *
       * @return The created bullet entity.
       */
      virtual std::shared_ptr<Entity> createBullet(int dir) = 0;

      /**
       * @brief Create barricade entity.
       *
       * @return The created barricade entity.
       */
      virtual std::shared_ptr<Entity> createBarricade() = 0;
  };

}

#endif
