#ifndef SPACEINVADERS_PIXELFACTORY_H
#define SPACEINVADERS_PIXELFACTORY_H

#include "../EntityRepresentationFactory.h"

namespace SpaceInvaders {

  class PixelFactory : public EntityRepresentationFactory
  {
    public:
      std::shared_ptr<EntityRepresentation> createInvaderRepresentation(const std::shared_ptr<Entity> &invader, int type);
      std::shared_ptr<EntityRepresentation> createTankRepresentation(const std::shared_ptr<Entity> &tank);
      std::shared_ptr<EntityRepresentation> createBulletRepresentation(const std::shared_ptr<Entity> &bullet);
      std::shared_ptr<EntityRepresentation> createBarricadeRepresentation(const std::shared_ptr<Entity> &barricade, int type);
      std::shared_ptr<Overlay> createOverlay(ScoreCounter *scoreCounter, LifeCounter *lifeCounter);
  };

}

#endif
