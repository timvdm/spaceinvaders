#include "PixelBarricade.h"
#include "PixelImages.h"
#include "../TextureManager.h"
#include "../Entity.h"
#include "../Notification.h"
#include "../impl/string.h"

#include <SFML/Graphics.hpp>

#include <cassert>

namespace SpaceInvaders {


  PixelBarricade::PixelBarricade(const std::shared_ptr<Entity> &entity, int type, const sf::Color &color)
    : EntityRepresentation(entity), m_sprite(0)
  {
    m_sprites.resize(4);
    // damage 0
    for (int damage = 0; damage < 4; ++damage) {
      sf::Image &img = impl::barricadeImage(type, damage, color);
      sf::Texture &tex = TextureManager::instance()->fromImage(make_string("barricade", type, "dmg", damage), img);
      m_sprites[damage].setTexture(tex);
    }

  }

  void PixelBarricade::notify(int message)
  {
    switch (message) {
      case NotifyHit:
        ++m_sprite;
        if (m_sprite > 3)
          m_sprite = 3;
        break;
      case NotifyRemoved:
        removeLater();
        break;
      default:
        break;
    }
  }

  void PixelBarricade::draw(sf::RenderWindow &window)
  {
    window.draw(m_sprites[m_sprite], entity()->transform());
  }

}
