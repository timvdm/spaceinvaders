#ifndef SPACEINVADERS_PIXELBARRICADE_H
#define SPACEINVADERS_PIXELBARRICADE_H

#include <SFML/Graphics/Sprite.hpp>

#include "../EntityRepresentation.h"

#include <vector>

namespace SpaceInvaders {

  class PixelBarricade : public EntityRepresentation
  {
    public:
      PixelBarricade(const std::shared_ptr<Entity> &entity, int type, const sf::Color &color);

      void draw(sf::RenderWindow &window);

      void notify(int message);

    private:
      std::vector<sf::Sprite> m_sprites;
      int m_sprite;
  };

}

#endif
