#include "PixelInvader.h"
#include "PixelImages.h"
#include "../TextureManager.h"
#include "../Entity.h"
#include "../Notification.h"
#include "../impl/string.h"

#include <SFML/Graphics.hpp>

namespace SpaceInvaders {


  PixelInvader::PixelInvader(const std::shared_ptr<Entity> &entity, int type, const sf::Color &color)
    : EntityRepresentation(entity)
  {
    // pose 1
    sf::Image &img1 = impl::invaderImage(type, 0, color);
    sf::Texture &tex1 = TextureManager::instance()->fromImage(make_string("invader", type, "a"), img1);
    m_sprite1.setTexture(tex1);
    // pose 2
    sf::Image &img2 = impl::invaderImage(type, 1, color);
    sf::Texture &tex2 = TextureManager::instance()->fromImage(make_string("invader", type, "b"), img2);
    m_sprite2.setTexture(tex2);

    m_onscreen = &m_sprite1;
    m_offscreen = &m_sprite2;
  }

  void PixelInvader::notify(int message)
  {
    switch (message) {
      case NotifyMoved:
        std::swap(m_onscreen, m_offscreen);
        break;
      case NotifyRemoved:
        removeLater();
        break;
      default:
        break;
    }
  }

  void PixelInvader::draw(sf::RenderWindow &window)
  {
    window.draw(*m_onscreen, entity()->transform());
  }

}
