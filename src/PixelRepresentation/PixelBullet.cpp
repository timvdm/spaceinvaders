#include "PixelBullet.h"
#include "PixelImages.h"
#include "../TextureManager.h"
#include "../Entity.h"
#include "../Notification.h"

#include <SFML/Graphics.hpp>

namespace SpaceInvaders {


  PixelBullet::PixelBullet(const std::shared_ptr<Entity> &entity, const sf::Color &color)
    : EntityRepresentation(entity)
  {
    sf::Image &img = impl::bulletImage(color);
    sf::Texture &tex = TextureManager::instance()->fromImage("bullet", img);
    m_sprite.setTexture(tex);
  }

  void PixelBullet::notify(int message)
  {
    switch (message) {
      case NotifyRemoved:
        removeLater();
        break;
      default:
        break;
    }
  }

  void PixelBullet::draw(sf::RenderWindow &window)
  {
    window.draw(m_sprite, entity()->transform());
  }

}
