#ifndef SPACEINVADERS_PIXELBULLET_H
#define SPACEINVADERS_PIXELBULLET_H

#include <SFML/Graphics/Sprite.hpp>

#include "../EntityRepresentation.h"

namespace SpaceInvaders {

  class PixelBullet : public EntityRepresentation
  {
    public:
      PixelBullet(const std::shared_ptr<Entity> &entity, const sf::Color &color);

      void draw(sf::RenderWindow &window);

      void notify(int message);

    private:
      sf::Sprite m_sprite;
  };

}

#endif
