/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "PixelOverlay.h"
#include <Config.h>
#include "../ScoreCounter.h"
#include "../LifeCounter.h"
#include <string>
#include <stdexcept>

namespace SpaceInvaders {

  PixelOverlay::PixelOverlay(ScoreCounter *scoreCounter, LifeCounter *lifeCounter)
    : Overlay(scoreCounter, lifeCounter)
  {
  }

  void PixelOverlay::init()
  {
    // load font
    if (!m_font.loadFromFile(std::string(FONTDIR) + "font.ttf"))
      throw std::runtime_error("Could not load font");

    // initialize score text
    m_score.setFont(m_font);
    m_score.setPosition(50, 10);
    m_score.setString("Score: 0");

    // intialize lives text
    m_lives.setFont(m_font);
    m_lives.setPosition(800, 10);
    m_lives.setString("Lives: 3");

    // intialize lost text
    m_messageRect.setSize(sf::Vector2f(300, 100));
    m_messageRect.setPosition(SCREEN_WIDTH /2 - 150, 300);
    m_messageRect.setFillColor(sf::Color(150, 150, 150));
    m_lostText.setFont(m_font);
    m_lostText.setPosition(430, 330);
    m_lostText.setColor(sf::Color(255, 255, 0));
    m_lostText.setString("You lost");

    // intialize earth
    m_earth.setSize(sf::Vector2f(SCREEN_WIDTH, 10));
    m_earth.setFillColor(sf::Color(0, 200, 0));
    m_earth.setPosition(0, SCREEN_HEIGHT - 10);
  }

  void PixelOverlay::draw(sf::RenderWindow &window)
  {
    // draw the score, lives and earth
    m_score.setString(scoreCounter()->scoreString());
    m_lives.setString(lifeCounter()->livesString());
    window.draw(m_score);
    window.draw(m_lives);
    window.draw(m_earth);

    if (lifeCounter()->lives() <= 0) {
      window.draw(m_messageRect);
      window.draw(m_lostText);
    }
  }

}
