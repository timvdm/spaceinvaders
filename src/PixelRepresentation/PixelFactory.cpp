#include "PixelFactory.h"
#include "PixelInvader.h"
#include "PixelTank.h"
#include "PixelBullet.h"
#include "PixelBarricade.h"
#include "PixelOverlay.h"

namespace SpaceInvaders {

  std::shared_ptr<EntityRepresentation> PixelFactory::createInvaderRepresentation(const std::shared_ptr<Entity> &invader, int type)
  {
    sf::Color color;
    switch (type) {
      case 0:
        color = sf::Color(255, 0, 0);
        break;
      case 1:
        color = sf::Color(0, 255, 0);
        break;
      case 2:
        color = sf::Color(0, 0, 255);
        break;
    }

    return std::shared_ptr<EntityRepresentation>(new PixelInvader(invader, type, color));
  }

  std::shared_ptr<EntityRepresentation> PixelFactory::createTankRepresentation(const std::shared_ptr<Entity> &tank)
  {
    return std::shared_ptr<EntityRepresentation>(new PixelTank(tank, sf::Color(0, 255, 255)));
  }

  std::shared_ptr<EntityRepresentation> PixelFactory::createBulletRepresentation(const std::shared_ptr<Entity> &bullet)
  {
    return std::shared_ptr<EntityRepresentation>(new PixelBullet(bullet, sf::Color(255, 255, 255)));
  }

  std::shared_ptr<EntityRepresentation> PixelFactory::createBarricadeRepresentation(const std::shared_ptr<Entity> &barricade, int type)
  {
    return std::shared_ptr<EntityRepresentation>(new PixelBarricade(barricade, type, sf::Color(0, 255, 255)));
  }

  std::shared_ptr<Overlay> PixelFactory::createOverlay(ScoreCounter *scoreCounter, LifeCounter *lifeCounter)
  {
    return std::shared_ptr<Overlay>(new PixelOverlay(scoreCounter, lifeCounter));
  }

}
