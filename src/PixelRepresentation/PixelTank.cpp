#include "PixelTank.h"
#include "PixelImages.h"
#include "../TextureManager.h"
#include "../Entity.h"

#include <SFML/Graphics.hpp>

namespace SpaceInvaders {


  PixelTank::PixelTank(const std::shared_ptr<Entity> &entity, const sf::Color &color)
    : EntityRepresentation(entity)
  {
    sf::Image &img = impl::tankImage(color);
    sf::Texture &tex = TextureManager::instance()->fromImage("tank", img);
    m_sprite.setTexture(tex);
  }

  void PixelTank::draw(sf::RenderWindow &window)
  {
    window.draw(m_sprite, entity()->transform());
  }

}
