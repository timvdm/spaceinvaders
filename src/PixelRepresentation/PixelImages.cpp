#include "PixelImages.h"
#include "../ImageManager.h"
#include <Config.h>
#include "../impl/string.h"

#include <cassert>

namespace SpaceInvaders {

  namespace impl {

    const char invader1a[8][12] = {{0,0,1,0,0,0,0,0,1,0,0,0},
                                   {0,0,0,1,0,0,0,1,0,0,0,0},
                                   {0,0,1,1,1,1,1,1,1,0,0,0},
                                   {0,1,1,0,1,1,1,0,1,1,0,0},
                                   {1,1,1,1,1,1,1,1,1,1,1,0},
                                   {1,0,1,1,1,1,1,1,1,0,1,0},
                                   {1,0,1,0,0,0,0,0,1,0,1,0},
                                   {0,0,0,1,1,0,1,1,0,0,0,0}};

    const char invader1b[8][12] = {{0,0,1,0,0,0,0,0,1,0,0,0},
                                   {1,0,0,1,0,0,0,1,0,0,1,0},
                                   {1,0,1,1,1,1,1,1,1,0,1,0},
                                   {1,1,1,0,1,1,1,0,1,1,1,0},
                                   {1,1,1,1,1,1,1,1,1,1,1,0},
                                   {0,0,1,1,1,1,1,1,1,0,0,0},
                                   {0,0,1,0,0,0,0,0,1,0,0,0},
                                   {0,1,0,0,0,0,0,0,0,1,0,0}};

    const char invader2a[8][12] = {{0,0,0,0,0,1,1,1,0,0,0,0},
                                   {0,0,0,0,1,1,1,1,1,0,0,0},
                                   {0,0,0,1,1,1,1,1,1,1,0,0},
                                   {0,0,1,1,0,1,1,1,0,1,1,0},
                                   {0,0,1,1,1,1,1,1,1,1,1,0},
                                   {0,0,0,1,0,1,1,1,0,1,0,0},
                                   {0,0,1,0,0,0,0,0,0,0,1,0},
                                   {0,0,0,1,0,0,0,0,0,1,0,0}};

    const char invader2b[8][12] = {{0,0,0,0,0,1,1,1,0,0,0,0},
                                   {0,0,0,0,1,1,1,1,1,0,0,0},
                                   {0,0,0,1,1,1,1,1,1,1,0,0},
                                   {0,0,1,1,0,1,1,1,0,1,1,0},
                                   {0,0,1,1,1,1,1,1,1,1,1,0},
                                   {0,0,0,0,1,0,0,0,1,0,0,0},
                                   {0,0,0,1,0,1,1,1,0,1,0,0},
                                   {0,0,1,0,1,0,0,0,1,0,1,0}};

    const char invader3a[8][12] = {{0,0,0,0,1,1,1,1,0,0,0,0},
                                   {0,1,1,1,1,1,1,1,1,1,1,0},
                                   {1,1,1,1,1,1,1,1,1,1,1,1},
                                   {1,1,1,0,0,1,1,0,0,1,1,1},
                                   {1,1,1,1,1,1,1,1,1,1,1,1},
                                   {0,0,0,1,1,0,0,1,1,0,0,0},
                                   {0,0,1,1,0,1,1,0,1,1,0,0},
                                   {1,1,0,0,1,0,0,0,0,0,1,1}};

    const char invader3b[8][12] = {{0,0,0,0,1,1,1,1,0,0,0,0},
                                   {0,1,1,1,1,1,1,1,1,1,1,0},
                                   {1,1,1,1,1,1,1,1,1,1,1,1},
                                   {1,1,1,0,0,1,1,0,0,1,1,1},
                                   {1,1,1,1,1,1,1,1,1,1,1,1},
                                   {0,0,1,1,1,0,0,1,1,1,0,0},
                                   {0,1,1,0,0,1,1,0,0,1,1,0},
                                   {0,0,1,1,0,0,0,0,1,1,0,0}};

    const char tank[7][13] = {{0,0,0,0,0,0,1,0,0,0,0,0,0},
                              {0,0,0,0,0,1,1,1,0,0,0,0,0},
                              {0,0,0,0,0,1,1,1,0,0,0,0,0},
                              {0,1,1,1,1,1,1,1,1,1,1,1,0},
                              {1,1,1,1,1,1,1,1,1,1,1,1,1},
                              {1,1,1,1,1,1,1,1,1,1,1,1,1},
                              {1,1,1,1,1,1,1,1,1,1,1,1,1}};

    const char barricade1dmg0[6][6] = {{1,1,1,1,1,1},
                                       {1,1,1,1,1,1},
                                       {1,1,1,1,1,1},
                                       {1,1,1,1,1,1},
                                       {1,1,1,1,1,1},
                                       {1,1,1,1,1,1}};

    const char barricade1dmg1[6][6] = {{0,0,1,1,1,1},
                                       {1,1,1,0,1,1},
                                       {1,0,1,1,1,1},
                                       {1,1,1,0,1,1},
                                       {1,1,0,1,1,1},
                                       {1,1,1,1,1,0}};

    const char barricade1dmg2[6][6] = {{0,0,1,1,0,0},
                                       {1,1,1,0,1,0},
                                       {1,0,1,0,1,1},
                                       {1,0,1,0,1,1},
                                       {0,0,0,1,1,1},
                                       {1,0,1,1,1,0}};

    const char barricade1dmg3[6][6] = {{0,0,0,1,0,0},
                                       {1,0,1,0,1,0},
                                       {0,0,1,0,0,1},
                                       {1,0,1,0,1,1},
                                       {0,0,0,1,0,1},
                                       {1,0,1,1,1,0}};

    const char barricade2dmg0[6][6] = {{0,0,0,1,1,1},
                                       {0,0,1,1,1,1},
                                       {0,1,1,1,1,1},
                                       {1,1,1,1,1,1},
                                       {1,1,1,1,1,1},
                                       {1,1,1,1,1,1}};

    const char barricade2dmg1[6][6] = {{0,0,0,1,1,1},
                                       {0,0,1,1,1,1},
                                       {0,1,1,0,1,1},
                                       {1,1,0,1,0,1},
                                       {1,0,1,1,1,1},
                                       {1,1,0,1,1,1}};

    const char barricade2dmg2[6][6] = {{0,0,0,0,1,1},
                                       {0,0,1,0,1,1},
                                       {0,1,1,0,0,1},
                                       {1,1,0,1,0,1},
                                       {1,0,1,0,1,1},
                                       {1,1,0,1,1,1}};

    const char barricade2dmg3[6][6] = {{0,0,0,0,1,1},
                                       {0,0,1,0,1,0},
                                       {0,1,0,0,0,1},
                                       {0,1,0,1,0,0},
                                       {0,0,1,0,1,1},
                                       {1,1,0,1,0,1}};

    const char barricade3dmg0[6][6] = {{1,1,1,0,0,0},
                                       {1,1,1,1,0,0},
                                       {1,1,1,1,1,0},
                                       {1,1,1,1,1,1},
                                       {1,1,1,1,1,1},
                                       {1,1,1,1,1,1}};

    const char barricade3dmg1[6][6] = {{1,1,1,0,0,0},
                                       {1,1,1,1,0,0},
                                       {1,1,0,1,1,0},
                                       {1,0,1,0,1,1},
                                       {1,0,1,1,0,1},
                                       {1,1,1,0,1,1}};

    const char barricade3dmg2[6][6] = {{1,1,0,0,0,0},
                                       {1,1,0,1,0,0},
                                       {1,0,0,1,1,0},
                                       {1,0,1,0,1,1},
                                       {1,1,0,1,0,1},
                                       {1,1,1,0,1,1}};

    const char barricade3dmg3[6][6] = {{1,1,0,0,0,0},
                                       {0,1,0,1,0,0},
                                       {1,0,0,0,1,0},
                                       {0,0,1,0,1,0},
                                       {1,1,0,1,0,0},
                                       {1,0,1,0,1,1}};

    const char barricade4dmg0[6][6] = {{1,1,1,1,1,1},
                                       {1,1,1,1,1,1},
                                       {1,1,1,1,0,0},
                                       {1,1,1,0,0,0},
                                       {1,1,0,0,0,0},
                                       {1,0,0,0,0,0}};

    const char barricade4dmg1[6][6] = {{1,0,1,0,1,1},
                                       {1,1,1,1,1,1},
                                       {1,0,1,0,0,0},
                                       {0,1,1,0,0,0},
                                       {1,0,0,0,0,0},
                                       {1,0,0,0,0,0}};

    const char barricade4dmg2[6][6] = {{1,0,1,0,1,0},
                                       {0,1,0,1,1,1},
                                       {1,0,1,0,0,0},
                                       {0,1,0,0,0,0},
                                       {1,0,0,0,0,0},
                                       {1,0,0,0,0,0}};

    const char barricade4dmg3[6][6] = {{0,0,1,0,1,0},
                                       {0,1,0,1,0,1},
                                       {1,0,0,0,0,0},
                                       {0,1,0,0,0,0},
                                       {0,0,0,0,0,0},
                                       {1,0,0,0,0,0}};

    const char barricade5dmg0[6][6] = {{1,1,1,1,1,1},
                                       {1,1,1,1,1,1},
                                       {0,0,1,1,1,1},
                                       {0,0,0,1,1,1,},
                                       {0,0,0,0,1,1,},
                                       {0,0,0,0,0,1}};

    const char barricade5dmg1[6][6] = {{1,1,0,1,0,1},
                                       {1,1,1,1,1,1},
                                       {0,0,0,1,0,1},
                                       {0,0,0,1,1,0},
                                       {0,0,0,0,0,1},
                                       {0,0,0,0,0,1}};

    const char barricade5dmg2[6][6] = {{0,1,0,1,0,1},
                                       {1,1,1,0,1,0},
                                       {0,0,0,1,0,1},
                                       {0,0,0,0,1,0},
                                       {0,0,0,0,0,1},
                                       {0,0,0,0,0,1}};

    const char barricade5dmg3[6][6] = {{0,1,0,1,0,0},
                                       {1,0,1,0,1,0},
                                       {0,0,0,0,0,1},
                                       {0,0,0,0,1,0},
                                       {0,0,0,0,0,0},
                                       {0,0,0,0,0,1}};

    const char box[11][11] = {{2,2,2,2,2,2,2,2,2,2,2},
                              {2,1,2,1,2,1,2,1,2,1,2},
                              {2,1,2,1,2,1,2,1,2,1,2},
                              {2,1,2,1,2,1,2,1,2,1,2},
                              {2,1,2,1,2,1,2,1,2,1,2},
                              {2,1,2,1,2,1,2,1,2,1,2},
                              {2,1,2,1,2,1,2,1,2,1,2},
                              {2,1,2,1,2,1,2,1,2,1,2},
                              {2,1,2,1,2,1,2,1,2,1,2},
                              {2,1,2,1,2,1,2,1,2,1,2},
                              {2,2,2,2,2,2,2,2,2,2,2}};

    const char* invaderFilename(int type, int pose)
    {
      switch(type) {
        case 0:
          if (pose)
            return "invader0b";
          return "invader0a";
        case 1:
          if (pose)
            return "invader1b";
          return "invader1a";
        case 2:
          if (pose)
            return "invader2b";
          return "invader2a";
      }
      return "";
    }

    sf::Image& invaderImage(int type, int pose, const sf::Color &color)
    {
      assert(type >= 0 && type <= 3);
      assert(pose == 0 || pose == 1);

      sf::Image &img = ImageManager::instance()->create(impl::invaderFilename(type, pose), 12 * PIXEL_SIZE, 8 * PIXEL_SIZE);

      auto ptr = impl::invader1a;

      switch (type) {
        case 0:
          switch (pose) {
            case 0:
              ptr = impl::invader1a;
              break;
            case 1:
              ptr = impl::invader1b;
              break;
          }
          break;
        case 1:
          switch (pose) {
            case 0:
              ptr = impl::invader2a;
              break;
            case 1:
              ptr = impl::invader2b;
              break;
          }
          break;
        case 2:
          switch (pose) {
            case 0:
              ptr = impl::invader3a;
              break;
            case 1:
              ptr = impl::invader3b;
              break;
          }
          break;
      }

      for (unsigned int i = 0; i < 12; ++i)
        for (unsigned int j = 0; j < 8; ++j)
          if (ptr[j][i])
            for (int x = 0; x < PIXEL_SIZE; ++x)
              for (int y = 0; y < PIXEL_SIZE; ++y)
                img.setPixel(i * PIXEL_SIZE + x, j * PIXEL_SIZE + y, color);

      return img;
    }

    sf::Image& tankImage(const sf::Color &color)
    {
      sf::Image &img = ImageManager::instance()->create("tank", 13 * PIXEL_SIZE, 7 * PIXEL_SIZE);

      for (unsigned int i = 0; i < 13; ++i)
        for (unsigned int j = 0; j < 7; ++j)
          if (impl::tank[j][i])
            for (int x = 0; x < PIXEL_SIZE; ++x)
              for (int y = 0; y < PIXEL_SIZE; ++y)
                img.setPixel(i * PIXEL_SIZE + x, j * PIXEL_SIZE + y, color);

      return img;
    }

    sf::Image& bulletImage(const sf::Color &color)
    {
      sf::Image &img = ImageManager::instance()->create("bullet", PIXEL_SIZE, 3 * PIXEL_SIZE);

      for (int i = 0; i < PIXEL_SIZE; ++i)
        for (int j = 0; j < 3 * PIXEL_SIZE; ++j)
          img.setPixel(i, j, color);

      return img;
    }

    /*
     * 2113
     * 1451
     * 1  1
     */
    sf::Image& barricadeImage(int type, int damage, const sf::Color &color)
    {
      sf::Image &img = ImageManager::instance()->create(make_string("barricade", type, "dmg", damage), 6 * PIXEL_SIZE, 6 * PIXEL_SIZE);

      auto ptr = barricade1dmg0;

      switch (type) {
        case 1:
          switch (damage) {
            case 0:
              ptr = barricade1dmg0;
              break;
            case 1:
              ptr = barricade1dmg1;
              break;
            case 2:
              ptr = barricade1dmg2;
              break;
            case 3:
              ptr = barricade1dmg3;
              break;
          }
          break;
        case 2:
          switch (damage) {
            case 0:
              ptr = barricade2dmg0;
              break;
            case 1:
              ptr = barricade2dmg1;
              break;
            case 2:
              ptr = barricade2dmg2;
              break;
            case 3:
              ptr = barricade2dmg3;
              break;
          }
          break;
        case 3:
          switch (damage) {
            case 0:
              ptr = barricade3dmg0;
              break;
            case 1:
              ptr = barricade3dmg1;
              break;
            case 2:
              ptr = barricade3dmg2;
              break;
            case 3:
              ptr = barricade3dmg3;
              break;
          }
          break;
        case 4:
          switch (damage) {
            case 0:
              ptr = barricade4dmg0;
              break;
            case 1:
              ptr = barricade4dmg1;
              break;
            case 2:
              ptr = barricade4dmg2;
              break;
            case 3:
              ptr = barricade4dmg3;
              break;
          }
          break;
        case 5:
          switch (damage) {
            case 0:
              ptr = barricade5dmg0;
              break;
            case 1:
              ptr = barricade5dmg1;
              break;
            case 2:
              ptr = barricade5dmg2;
              break;
            case 3:
              ptr = barricade5dmg3;
              break;
          }
          break;
      }

      for (unsigned int i = 0; i < 6; ++i)
        for (unsigned int j = 0; j < 6; ++j)
          if (ptr[j][i])
            for (int x = 0; x < PIXEL_SIZE; ++x)
              for (int y = 0; y < PIXEL_SIZE; ++y)
                img.setPixel(i * PIXEL_SIZE + x, j * PIXEL_SIZE + y, color);

      return img;
    }

    sf::Image& boxImage(const sf::Color &light, const sf::Color &dark)
    {
      sf::Image &img = ImageManager::instance()->create("box", 11 * PIXEL_SIZE, 11 * PIXEL_SIZE);

      for (unsigned int i = 0; i < 11; ++i)
        for (unsigned int j = 0; j < 11; ++j)
          if (box[j][i])
            for (int x = 0; x < PIXEL_SIZE; ++x)
              for (int y = 0; y < PIXEL_SIZE; ++y)
                img.setPixel(i * PIXEL_SIZE + x, j * PIXEL_SIZE + y, box[j][i] == 1 ? light  : dark);

      return img;
    }


  }

}
