#ifndef SPACEINVADERS_PIXELTANK_H
#define SPACEINVADERS_PIXELTANK_H

#include <SFML/Graphics/Sprite.hpp>

#include "../EntityRepresentation.h"

namespace SpaceInvaders {

  class PixelTank : public EntityRepresentation
  {
    public:
      PixelTank(const std::shared_ptr<Entity> &entity, const sf::Color &color);

      void draw(sf::RenderWindow &window);

    private:
      sf::Sprite m_sprite;
  };

}

#endif
