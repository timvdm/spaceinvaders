#ifndef SPACEINVADERS_PIXELIMAGES_H
#define SPACEINVADERS_PIXELIMAGES_H

#include <SFML/Graphics/Image.hpp>

namespace SpaceInvaders {

  namespace impl {

    sf::Image& invaderImage(int type, int pose, const sf::Color &color);

    sf::Image& tankImage(const sf::Color &color);

    sf::Image& bulletImage(const sf::Color &color);

    /**
     *
     * 2113
     * 1451
     * 1  1
     *
     */
    sf::Image& barricadeImage(int type, int damage, const sf::Color &color);

    sf::Image& boxImage(const sf::Color &light, const sf::Color &dark);

  }

}

#endif
