#ifndef SPACEINVADERS_PIXELINVADER_H
#define SPACEINVADERS_PIXELINVADER_H

#include <SFML/Graphics/Sprite.hpp>

#include "../EntityRepresentation.h"

namespace SpaceInvaders {

  class PixelInvader : public EntityRepresentation
  {
    public:
      PixelInvader(const std::shared_ptr<Entity> &entity, int type, const sf::Color &color);

      void draw(sf::RenderWindow &window);

      void notify(int message);

    private:
      sf::Sprite m_sprite1;
      sf::Sprite m_sprite2;
      sf::Sprite *m_onscreen;
      sf::Sprite *m_offscreen;
  };

}

#endif
