#include "InvaderAI.h"
#include "../Entity.h"
#include <Config.h>

#include <cstdlib>

namespace SpaceInvaders {

  InvaderAI::InvaderAI(EntityFactory *entityFactory,
      EntityRepresentationFactory *representationFactory)
    : InvaderController(entityFactory, representationFactory),
      m_counter(0), m_dir(1), m_speed(1.0), m_fireSpeed(1.0), m_elapsed(0.0), m_fireElapsed(0.0)
  {
  }

  void InvaderAI::update(double elapsed)
  {
    m_elapsed += elapsed;
    m_fireElapsed += elapsed;

    if (m_elapsed > 1.0 / m_speed) {
      m_counter += m_dir;
      if (m_counter > 60) {
        for (auto invader : invaders())
          if (auto ptr = invader.lock())
            ptr->move(0, PIXEL_SIZE * 14);
        m_dir = -1;
      }
      if (m_counter < 0) {
        for (auto invader : invaders())
          if (auto ptr = invader.lock())
            ptr->move(0, PIXEL_SIZE * 14);
        m_dir = 1;
      }

      for (auto invader : invaders())
          if (auto ptr = invader.lock())
            ptr->move(4 * m_dir, 0);

      m_elapsed -= 1.0 / m_speed;
    }

    if (m_fireElapsed > 1.0 / m_fireSpeed) {

      // make list of invaders in bottom row
      int lowest = 0;
      std::vector<std::shared_ptr<Entity>> lowestPtrs;
      for (auto invader : invaders())
        if (auto ptr = invader.lock()) {
          if (ptr->position().y > lowest) {
            lowestPtrs.clear();
            lowest = ptr->position().y;
            lowestPtrs.push_back(ptr);
          } else if (ptr->position().y == lowest) {
            lowestPtrs.push_back(ptr);
          }
        }

      if (lowestPtrs.size())
        fire(lowestPtrs[rand() % lowestPtrs.size()]);

      m_fireElapsed -= 1.0 / m_fireSpeed;
    }

  }

}
