#ifndef SPACEINVADERS_HUMANTANKCONTROLLER_H
#define SPACEINVADERS_HUMANTANKCONTROLLER_H

#include "../TankController.h"

namespace SpaceInvaders {

  class Entity;

  class HumanTankController : public TankController
  {
    public:
      HumanTankController(int leftBorder, int rightBorder,
          EntityFactory *entityFactory,
          EntityRepresentationFactory *representationFactory);

      void update(double elapsed);
  };

}

#endif
