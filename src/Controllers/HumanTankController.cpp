#include "HumanTankController.h"
#include "../Entity.h"

#include <SFML/Window/Keyboard.hpp>

namespace SpaceInvaders {

  HumanTankController::HumanTankController(int leftBorder, int rightBorder,
      EntityFactory *entityFactory, EntityRepresentationFactory *representationFactory)
    : TankController(leftBorder, rightBorder, entityFactory, representationFactory)
  {
  }

  void HumanTankController::update(double elapsed)
  {
    double speed = 300.0;

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
      left(speed * elapsed);

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
      right(speed * elapsed);

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
      fire();
  }

}
