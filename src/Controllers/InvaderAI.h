#ifndef SPACEINVADERS_INVADERAI_H
#define SPACEINVADERS_INVADERAI_H

#include "../InvaderController.h"

namespace SpaceInvaders {

  class InvaderAI : public InvaderController
  {
    public:
      InvaderAI(EntityFactory *entityFactory,
          EntityRepresentationFactory *representationFactory);

      void update(double elapsed);

      int speed() const
      {
        return m_speed;
      }

      void setSpeed(int speed)
      {
        m_speed = speed;
      }

      int fireSpeed() const
      {
        return m_fireSpeed;
      }

      void setFireSpeed(int fireSpeed)
      {
        m_fireSpeed = fireSpeed;
      }


    private:
      int m_counter;
      int m_dir;
      int m_speed;
      int m_fireSpeed;
      double m_elapsed;
      double m_fireElapsed;
  };

}

#endif
