/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SPACEINVADERS_ENTITYREPRESENTATIONFACTORY_H
#define SPACEINVADERS_ENTITYREPRESENTATIONFACTORY_H

#include <memory>

namespace SpaceInvaders {

  /**
   * @file EntityRepresentationFactory.h
   */

  // forward declarations
  class Entity;
  class EntityRepresentation;
  class Overlay;
  class ScoreCounter;
  class LifeCounter;

  /**
   * @brief Base class for entity representation factories.
   */
  class EntityRepresentationFactory
  {
    public:
      /**
       * @brief Destructor.
       */
      virtual ~EntityRepresentationFactory();

      /**
       * @brief Create invader entity representation.
       *
       * @param invader The invader entity.
       * @param type The invader type (i.e. 1, 2 or 3).
       *
       * @return The created invader entity representation.
       */
      virtual std::shared_ptr<EntityRepresentation> createInvaderRepresentation(const std::shared_ptr<Entity> &invader, int type) = 0;

      /**
       * @brief Create tank entity representation.
       *
       * @param tank The tank entity.
       *
       * @return The created tank entity representation.
       */
      virtual std::shared_ptr<EntityRepresentation> createTankRepresentation(const std::shared_ptr<Entity> &tank) = 0;

      /**
       * @brief Create bullet entity representation.
       *
       * @param bullet The bullet entity.
       *
       * @return The created bullet entity representation.
       */
      virtual std::shared_ptr<EntityRepresentation> createBulletRepresentation(const std::shared_ptr<Entity> &bullet) = 0;

      /**
       * @brief Create barricade entity representation.
       *
       * @param barricade The barricade entity.
       * @param type The barricade (location) type.
       *
       * @return The created barricade entity representation.
       */
      virtual std::shared_ptr<EntityRepresentation> createBarricadeRepresentation(const std::shared_ptr<Entity> &barricade, int type) = 0;

      /**
       * @brief Create overlay representation.
       *
       * @param scoreCounter The score counter.
       * @param lifeCounter The life counter.
       *
       * @return The created overlay representation.
       */
      virtual std::shared_ptr<Overlay> createOverlay(ScoreCounter *scoreCounter, LifeCounter *lifeCounter) = 0;
  };

}

#endif
