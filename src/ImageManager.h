/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SPACEINVADERS_IMAGEMANAGER_H
#define SPACEINVADERS_IMAGEMANAGER_H

#include <SFML/Graphics/Image.hpp>

#include <map>

namespace SpaceInvaders {

  /**
   * @file ImageManager.h
   */

  /**
   * @brief Class for managing SFML images.
   *
   * The ImageManager class allows images to shared between other parts of the
   * code without duplicating them. Like all manager classes, the ImageManager
   * is a singleton and an instance can be obtained as illustarted below:
   *
   @code
   ImageManager *imageManager = ImageManager::instance();
   @endcode
   */
  class ImageManager
  {
    public:
      /**
       * @brief Get the singleton image manager instance.
       *
       * @return The image manager.
       */
      static ImageManager* instance();

      /**
       * @brief Set the image directory.
       *
       * When searching for images (after calling ImageManager::image()), the
       * image manager will search this directory for the image. By default,
       * the image directory is the current working directory.
       *
       * @param directory The new image directory.
       */
      void setImageDirectory(const std::string &directory)
      {
        m_directory = directory;
      }

      /**
       * @brief Get the image with the specified filename.
       *
       * If the image is not found, a std::runtime_error exception will be
       * thrown.
       *
       * @param filename The image filename.
       *
       * @return A reference to the image.
       */
      sf::Image& image(const std::string &filename);

      /**
       * @brief Create a new in-memory image.
       *
       * @param filename The filename (for later retrieval using
       *        ImageManager::image()).
       * @param width The image width.
       * @param height The image height.
       *
       * @return A reference to the image.
       */
      sf::Image& create(const std::string &filename, int width, int height);

    private:
      /**
       * @brief Constructor.
       *
       * This constructor is private to ensure only a single instance can exist.
       * To obtain a pointer to the singleton instance, use the static
       * ImageManager::instance() function.
       */
      ImageManager();

      /**
       * @brief Implementation for loading images from disk.
       *
       * @param filename The image filename.
       */
      void loadImage(const std::string &filename);

      std::string m_directory; //!< The image directory.
      std::map<std::string, sf::Image> m_images; //!< The images.
  };

}

#endif
