/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "InvaderController.h"
#include <Config.h>
#include "Entity.h"
#include "EntityManager.h"
#include "EntityFactory.h"
#include "EntityRepresentation.h"
#include "EntityRepresentationFactory.h"
#include "EntityRepresentationManager.h"

namespace SpaceInvaders {

  void InvaderController::fire(const std::shared_ptr<Entity> &invader)
  {
    std::shared_ptr<Entity> bullet = m_entityFactory->createBullet(1);
    auto pos = invader->position();
    auto box = invader->boundingBox();
    bullet->setPosition(pos.x + (box.x - PIXEL_SIZE) / 2, pos.y + box.y);
    EntityManager::instance()->addEntity(bullet);
    std::shared_ptr<EntityRepresentation> repr = m_representationFactory->createBulletRepresentation(bullet);
    bullet->addObserver(repr);
    EntityRepresentationManager::instance()->addRepresentation(repr);
  }

  bool InvaderController::invadersWon(int goal)
  {
    for (auto invader : invaders())
      if (auto ptr = invader.lock())
        if (ptr->position().y > goal)
          return true;
    return false;
  }

  bool InvaderController::invadersLost()
  {
    for (auto invader : invaders())
      if (auto ptr = invader.lock())
        return false;
    return true;
  }

}
