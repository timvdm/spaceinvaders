/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SPACEINVADERS_ENTITYMANAGER_H
#define SPACEINVADERS_ENTITYMANAGER_H

#include <vector>
#include <memory>

namespace SpaceInvaders {

  /**
   * @file EntityManager.h
   */

  // forward declaration
  class Entity;

  /**
   * @brief Class for managing entities.
   *
   * All entities in the game need to be added to the entity manager. The entity
   * manager will update entities and performs other bookkeeping tasks. All
   * manager classes are singletons (with private constructors) to ensure only
   * one manager of each type exists. To obtain a pointer to the instance, the
   * static EntityManager::instance() can be used:
   *
   @code
   EntityManager *entityManager = EntityManager::instance();
   @endcode
   *
   * This pointer must never be deleted.
   *
   * Entities are added to the manager using EntityManager::addEntity() and can
   * be removed indirectly by calling Entity::removeLater(). The EntityManager
   * does not remove the entity directly. Instead, at the end of an update, the
   * EntityManager::cleanup() function removes all entities that were scheduled
   * to be removed during the update. This ensures a consistent view of entities
   * during an update.
   *
   * A typical usage of the manager classes is given in the example below:
   @code
   sf::Clock clock;
   while (true) {
     // get elapsed time
     sf::Time elapsed = clock.restart();

     // update managers
     EntityManager::instance()->update(elapsed.asSeconds());

     // perform other tasks such as collision detection, game logic, ...

     // call manager cleanup fucntions
     EntityManager::instance()->cleanup();
   }
   @endcode
   *
   * All entities and entity representations use shared pointers to make memory
   * management easier and safer. For example, and entity might be hit and removed
   * while the representation lives slightly longer to show an explosion animation.
   * The use of shared pointers ensures the Entity can still be accessed by the
   * EntityRepresentation after it has been removed ("deleted") from the entity
   * manager.
   */
  class EntityManager
  {
    public:
      /**
       * @brief Get the singleton entity manager instance.
       *
       * @return The entity manager.
       */
      static EntityManager* instance();

      /**
       * @brief Add an entity.
       *
       * @param entity The entity to add.
       */
      void addEntity(const std::shared_ptr<Entity> &entity);

      /**
       * @brief Get the entities.
       *
       * @return The entities.
       */
      const std::vector<std::shared_ptr<Entity>>& entities() const
      {
        return m_entities;
      }

      /**
       * @brief Update all entities.
       *
       * @param elapsed The elapsed time since the last update in seconds.
       */
      void update(double elapsed);

      /**
       * @brief The cleanup function to be called at the end of an update.
       *
       * This function removes the entities that were scheduled to be removed
       * during the update.
       */
      void cleanup();

    private:
      // needed for removeLater() below
      friend class Entity;

      /**
       * @brief Schedule an entity to be removed.
       *
       * @param entity The entity.
       */
      void removeLater(Entity *entity);

      /**
       * @brief Constructor.
       *
       * This constructor is private to ensure only a single instance can exist.
       * To obtain a pointer to the singleton instance, use the static
       * EntityManager::instance() function.
       */
      EntityManager();

      std::vector<std::shared_ptr<Entity>> m_entities; //!< The entities.
      std::vector<Entity*> m_removedEntities; //!< Entities to be removed.
  };

}

#endif
