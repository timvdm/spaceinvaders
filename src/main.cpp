#include <SFML/Graphics.hpp>

#include "EntityRepresentationManager.h"
#include "EntityManager.h"

#include <iostream>
#include <sstream>

#include <Config.h>
#include "Entity.h"
#include "EntityRepresentation.h"
#include "Overlay.h"
#include "PixelRepresentation/PixelFactory.h"

#include "ScoreCounter.h"
#include "LifeCounter.h"

#include "Entities/DefaultEntityFactory.h"
#include "Entities/Invader.h"
#include "Entities/Bullet.h"

#include "Controllers/InvaderAI.h"
#include "Controllers/HumanTankController.h"

#include "Round.h"

using namespace SpaceInvaders;


int main()
{
  // create window
  sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "SpaceInvaders");

  // create factories
  EntityFactory *entityFactory = new DefaultEntityFactory;
  EntityRepresentationFactory *representationFactory = new PixelFactory;

  std::shared_ptr<ScoreCounter> scoreCounter(new ScoreCounter);
  std::shared_ptr<LifeCounter> lifeCounter(new LifeCounter);

  int speed = 3;
  int fireSpeed = 1;
  while (window.isOpen()) {

    // create invader controller
    InvaderAI *invaderController = new InvaderAI(entityFactory, representationFactory);
    invaderController->setSpeed(speed++);
    invaderController->setFireSpeed(fireSpeed++);

    // initialize tank controller
    TankController  *tankController = new HumanTankController(TANK_BORDER, SCREEN_WIDTH - 13 * PIXEL_SIZE - TANK_BORDER, entityFactory, representationFactory);

    Round round(entityFactory, representationFactory, invaderController, tankController, scoreCounter, lifeCounter);
    round.init();

    // run as long as window is open
    while (window.isOpen()) {
      // check for events
      sf::Event event;
      while (window.pollEvent(event)) {
        if (event.type == sf::Event::Closed) {
          window.close();
        }
      }

      // quit on escape
      if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        window.close();

      // update round (logic + graphics)
      round.update(window);

      // end current frame
      window.display();
    }

  }

  return 0;
}
