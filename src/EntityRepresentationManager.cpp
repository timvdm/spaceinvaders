/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "EntityRepresentationManager.h"
#include "EntityRepresentation.h"
#include "impl/util.h"

#include <cassert>
#include <algorithm>

namespace SpaceInvaders {

  EntityRepresentationManager::EntityRepresentationManager()
  {
  }

  EntityRepresentationManager* EntityRepresentationManager::instance()
  {
    // singleton instance
    static EntityRepresentationManager *obj = 0;

    // create the instance if it does not exist
    if (!obj)
      obj = new EntityRepresentationManager;

    return obj;
  }

  void EntityRepresentationManager::addRepresentation(const std::shared_ptr<EntityRepresentation> &representation)
  {
    m_representations.push_back(representation);
  }

  void EntityRepresentationManager::removeLater(EntityRepresentation *representation)
  {
    m_removedRepresentations.push_back(representation);
  }

  void EntityRepresentationManager::cleanup()
  {
    // remove all entity representations that called removeLater() in the update
    for (auto representation : m_removedRepresentations) {
      auto iter = std::find_if(m_representations.begin(), m_representations.end(), CompareSharedPtr<EntityRepresentation>(representation));
      if (iter != m_representations.end())
        m_representations.erase(iter);
    }
    m_removedRepresentations.clear();
  }

  void EntityRepresentationManager::draw(sf::RenderWindow &window)
  {
    // draw all entity representations
    for (auto representation : m_representations)
      representation->draw(window);
  }

}
