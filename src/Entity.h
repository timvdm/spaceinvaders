/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SPACEINVADERS_ENTITY_H
#define SPACEINVADERS_ENTITY_H

#include "Subject.h"

#include <SFML/Graphics/Transformable.hpp>

namespace SpaceInvaders {

  /**
   * @file Entity.h
   */

  // forward declaration
  class EntityRepresentation;

  /**
   * @brief Base class for entities.
   *
   * All logical elements (i.e. invaders, bullets, ...) in the game are
   * represented using entities. In the Model-View-Controller pattern, entities
   * are the model. The views (i.e. EntityRepresentation) are notified using
   * the observer design pattern (i.e. Entity inherits Subject and
   * EntityRepresentation inherits Observer).
   */
  class Entity : public Subject
  {
    public:
      /**
       * @brief Constructor.
       */
      Entity();

      /**
       * @brief Destructor.
       */
      virtual ~Entity();

      /**
       * @brief Get the entity bounding box.
       *
       * An entity representation needs to adhere to this drawing box.
       *
       * @return The entity bounding box.
       */
      virtual sf::Vector2f boundingBox() const = 0;

      /**
       * @brief Schedule this entity to be removed.
       *
       * The entity will be removed and removed from the EntityManager when
       * EntityManager::cleanup() is called at the end of a frame.
       */
      void removeLater();

      /**
       * @brief Update the entity.
       *
       * Entities are updates each frame when the EntityManager::update()
       * function is executed.
       *
       * @param elapsed The elapsed time since the last update.
       */
      virtual void update(double elapsed) = 0;

      /**
       * @brief Get the entity transform.
       *
       * @return The entity transform.
       */
      const sf::Transform& transform() const
      {
        return m_transform.getTransform();
      }

      /**
       * @brief Get the entity position.
       *
       * @return The entity position.
       */
      sf::Vector2f position() const
      {
        return m_transform.getPosition();
      }

      /**
       * @brief Set the entity position.
       *
       * All observers will receive the NotifyMove notification.
       *
       * @param x The new x coordinate.
       * @param y The new y coordinate.
       */
      void setPosition(float x, float y);

      /**
       * @brief Set the entity position.
       *
       * All observers will receive the NotifyMove notification.
       *
       * @param position The new position.
       */
      void setPosition(const sf::Vector2f &position);

      /**
       * @brief Move the entity relative to the current position.
       *
       * All observers will receive the NotifyMove notification.
       *
       * @param offsetX The x offset.
       * @param offsetY The y offset.
       */
      void move(float offsetX, float offsetY);

      /**
       * @brief Move the entity relative to the current position.
       *
       * All observers will receive the NotifyMove notification.
       *
       * @param offset The offset.
       */
      void move(const sf::Vector2f &offset);

      /**
       * @brief Get the damage that this entity does when it hits other entities.
       *
       * Entities that do damage (e.g. bullets) should reimplement this function
       * to return the damage. The default implementation returns 0.
       *
       * @return The damage done by this entity.
       */
      virtual int damage() const;

      /**
       * @brief The entity was hit by another entity.
       *
       * This function is called on an entity when it is hit by another entity.
       * Entities that can take damage should reimplement this function. The
       * default implementation does nothing.
       *
       * @param damage The damage done by the other entity.
       */
      virtual void hit(int damage);

    private:
      sf::Transformable m_transform; //!< The entity transformation.
  };

}

#endif
