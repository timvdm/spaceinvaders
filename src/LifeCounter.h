/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SPACEINVADERS_LIFECOUNTER_H
#define SPACEINVADERS_LIFECOUNTER_H

#include "Observer.h"
#include <string>

namespace SpaceInvaders {

  /**
   * @file LifeCounter.h
   */

  /**
   * @brief Class for keeping track of the lives.
   */
  class LifeCounter : public Observer
  {
    public:
      /**
       * @brief Constructor.
       *
       * @brief lives The initial number of lives.
       */
      LifeCounter(int lives = 3);

      /**
       * @brief Observer::notify() implementation.
       *
       * If the message equals NotifyHit, one life will be subtracted. Other
       * messages are ignored.
       *
       * @param message The notification message (i.e. NotifyHit).
       */
      void notify(int message);

      /**
       * @brief Get the number of lives.
       *
       * @return The number of lives.
       */
      int lives() const
      {
        return m_lives;
      }

      /**
       * @brief Set the number of lives,
       *
       * @param value The new number of lives.
       */
      void setLives(int value)
      {
        m_lives = value;
      }

      /**
       * @brief Get the number of lives as a string.
       *
       * This function returns a string of the form "Lives: x" where x is the
       * number of lives.
       *
       * @return The number of lives string.
       */
      std::string livesString() const;

    private:
      int m_lives; //!< The number of lives.
  };

}

#endif
