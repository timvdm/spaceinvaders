/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SPACEINVADERS_TEXTUREMANAGER_H
#define SPACEINVADERS_TEXTUREMANAGER_H

#include <SFML/Graphics/Texture.hpp>

#include <map>

namespace SpaceInvaders {

  /**
   * @file TextureManager.h
   */

  /**
   * @brief Class for managing SFML textures.
   *
   * The TextureManager class allows textures to shared between other parts of the
   * code without duplicating them. Like all manager classes, the TextureManager
   * is a singleton and an instance can be obtained as illustarted below:
   *
   @code
   TextureManager *textureManager = TextureManager::instance();
   @endcode
   */
  class TextureManager
  {
    public:
      /**
       * @brief Get the singleton texture manager instance.
       *
       * @return The texture manager.
       */
      static TextureManager* instance();

      /**
       * @brief Set the texture directory.
       *
       * When searching for textures (after calling TextureManager::texture()), the
       * texture manager will search this directory for the texture. By default,
       * the texture directory is the current working directory.
       *
       * @param directory The new texture directory.
       */
      void setTextureDirectory(const std::string &directory)
      {
        m_directory = directory;
      }

      /**
       * @brief Get the texture with the specified filename.
       *
       * If the texture is not found, a std::runtime_error exception will be
       * thrown.
       *
       * @param filename The texture filename.
       *
       * @return A reference to the texture.
       */
      sf::Texture& texture(const std::string &filename);

      /**
       * @brief Create a new texture from a SFML image.
       *
       * @param filename The texture filename (for later retrieval using
       *        TextureManager::texture()).
       * @param image The SFML image.
       *
       * @return A reference to the texture.
       */
      sf::Texture& fromImage(const std::string &filename, const sf::Image &image);

    private:
      /**
       * @brief Constructor.
       *
       * This constructor is private to ensure only a single instance can exist.
       * To obtain a pointer to the singleton instance, use the static
       * TextureManager::instance() function.
       */
      TextureManager();

      /**
       * @brief Implementation for loading textures from disk.
       *
       * @param filename The texture filename.
       */
      void loadTexture(const std::string &filename);

      std::string m_directory; //!< The texture directory.
      std::map<std::string, sf::Texture> m_textures; //!< The textures.
  };

}

#endif
