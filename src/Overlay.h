/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SPACEINVADERS_OVERLAY_H
#define SPACEINVADERS_OVERLAY_H

// forward declaration
namespace sf {
  class RenderWindow;
}

namespace SpaceInvaders {

  /**
   * @file Overlay.h
   */

  // forward declarations
  class ScoreCounter;
  class LifeCounter;

  /**
   * @brief Base class for overlay representations.
   *
   * The overlay for dwaring text, score, lifes etc. is no an entity repsentation
   * since there is no associated entity. However, in most cases the overlay should
   * match the theme from the entity representations and this base class is provided
   * for this purpose.
   */
  class Overlay
  {
    public:
      /**
       * @brief Constructor.
       */
      Overlay(ScoreCounter *scoreCounter, LifeCounter *lifeCounter);

      /**
       * @brief Destructor.
       */
      virtual ~Overlay();

      /**
       * @brief Get the score counter.
       *
       * @return The score counter.
       */
      ScoreCounter* scoreCounter() const
      {
        return m_scoreCounter;
      }

      /**
       * @brief Get the life counter.
       *
       * @return The life counter.
       */
      LifeCounter* lifeCounter() const
      {
        return m_lifeCounter;
      }

      /**
       * @brief Initialize the overlay.
       *
       * This function is called once before the overlay is used
       * (i.e. Overlay::draw() is called) for the first time.
       */
      virtual void init() = 0;

      /**
       * @brief Update the overlay.
       *
       * @param window The SFML render window.
       */
      virtual void draw(sf::RenderWindow &window) = 0;

    private:
      ScoreCounter *m_scoreCounter;
      LifeCounter *m_lifeCounter;
  };

}

#endif
