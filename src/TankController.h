/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SPACEINVADERS_TANKCONTROLLER_H
#define SPACEINVADERS_TANKCONTROLLER_H

#include <vector>
#include <memory>

#include <SFML/System/Clock.hpp>

namespace SpaceInvaders {

  /**
   * @file TankController.h
   */

  // forward declaration
  class Entity;
  class EntityFactory;
  class EntityRepresentationFactory;

  /**
   * @brief Base class for tank controllers.
   */
  class TankController
  {
    public:
      /**
       * @brief Constructor.
       *
       * @param leftBorder The left border where the tank can never be.
       * @param rightBorder The right border where the tank can never be.
       * @param entityFactory The entity factory for creating bullets.
       * @param representationFactory The representation factory for creating
       *        bullet representations.
       */
      TankController(int leftBorder, int rightBorder,
          EntityFactory *entityFactory,
          EntityRepresentationFactory *representationFactory);

      /**
       * @brief Destructor.
       */
      virtual ~TankController();

      /**
       * @brief Get the tank.
       *
       * The controller only keeps a std::weak_ptr to the tank so this function
       * may return 0.
       *
       * @return The tank.
       */
      Entity* tank() const;

      /**
       * @brief Set the tank to control.
       *
       * @param tank The tank.
       */
      void setTank(const std::shared_ptr<Entity> &tank);

      /**
       * @brief Update the controller.
       *
       * Concrete tank controllers need to implement this function.
       *
       * @param elapsed The elapsed time in seconds.
       */
      virtual void update(double elapsed) = 0;

      /**
       * @brief Move the tank left.
       *
       * This function will never move the tank outside the specified borders.
       *
       * @param x The offset to move the tank.
       */
      void left(float x);

      /**
       * @brief Move the tank right.
       *
       * This function will never move the tank outside the specified borders.
       *
       * @param x The offset to move the tank.
       */
      void right(float x);

      /**
       * @brief Fire a bullet.
       *
       * If the tank is still reloading, this function does nothing. Otherwise,
       * this function creates a bullet and representation and adds these to
       * their corresponding managers.
       */
      void fire();

    private:
      std::weak_ptr<Entity> m_tank; //!< The tank.
      int m_leftBorder; //!< The left border.
      int m_rightBorder; //!< The right border.
      EntityFactory *m_entityFactory; //!< The entity factory.
      EntityRepresentationFactory *m_representationFactory; //!< The representation factory.
      sf::Clock m_clock; //!< The clock for reload timing.
  };

}

#endif
