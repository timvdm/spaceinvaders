#ifndef SPACEINVADERS_IMPL_UTIL_H
#define SPACEINVADERS_IMPL_UTIL_H

#include <memory>

namespace SpaceInvaders {

  template<typename T>
  class CompareSharedPtr
  {
    public:
      CompareSharedPtr(T *ptr) : m_ptr(ptr)
      {
      }

      bool operator()(const std::shared_ptr<T> &ptr)
      {
        return ptr.get() == m_ptr;
      }

    private:
      T *m_ptr;
  };

  template<typename T>
  class CompareWeakPtr
  {
    public:
      CompareWeakPtr(T *ptr) : m_ptr(ptr)
      {
      }

      bool operator()(const std::weak_ptr<T> &ptr)
      {
        auto p = ptr.lock();
        if (p)
          return p.get() == m_ptr;
        return false;
      }

    private:
      T *m_ptr;
  };

}

#endif
