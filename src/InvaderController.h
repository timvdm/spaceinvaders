/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SPACEINVADERS_INVADERCONTROLLER_H
#define SPACEINVADERS_INVADERCONTROLLER_H

#include <vector>
#include <memory>

namespace SpaceInvaders {

  /**
   * @file InvaderController.h
   */

  // forward declaration
  class Entity;
  class EntityFactory;
  class EntityRepresentationFactory;

  /**
   * @brief Base class for invader controllers.
   */
  class InvaderController
  {
    public:
      /**
       * @brief Contstructor.
       *
       * @param entityFactory The entity factory for creating bullets.
       * @param representationFactory The entity representation factory for
       *        creating bullets.
       */
      InvaderController(EntityFactory *entityFactory,
          EntityRepresentationFactory *representationFactory)
        : m_entityFactory(entityFactory),
          m_representationFactory(representationFactory)
      {
      }

      /**
       * @brief Destructor.
       */
      virtual ~InvaderController()
      {
      }

      /**
       * @brief Get the invaders.
       *
       * @return The invaders.
       */
      const std::vector<std::weak_ptr<Entity>>& invaders() const
      {
        return m_invaders;
      }

      /**
       * @overload
       */
      std::vector<std::weak_ptr<Entity>>& invaders()
      {
        return m_invaders;
      }

      /**
       * @brief Clear the list of invaders.
       */
      void clearInvaders()
      {
        m_invaders.clear();
      }

      /**
       * @brief Add an invader to the controller.
       *
       * @param invader The invader to add.
       */
      void addInvader(const std::weak_ptr<Entity> &invader)
      {
        m_invaders.push_back(invader);
      }

      /**
       * @brief Update the controller.
       *
       * This function should be implemented by subclasses to control the
       * invaders.
       *
       * @param elapsed The elapsed time since the last update in seconds.
       */
      virtual void update(double elapsed) = 0;

      /**
       * @brief Fire a bullet from @p invader.
       *
       * This function can be used by subclasses to fire.
       *
       * @param invader The invader that shoots.
       */
      void fire(const std::shared_ptr<Entity> &invader);

      /**
       * @brief Check if the invaders won (i.e. they have landed).
       *
       * @param goal The Y value when the invaders reach earth.
       *
       * @return True if the invaders won.
       */
      bool invadersWon(int goal);

      /**
       * @brief Check if there are still invaders left.
       *
       * @return True if all invaders are destroyed.
       */
      bool invadersLost();

    private:
      std::vector<std::weak_ptr<Entity>> m_invaders; //!< The invaders.
      EntityFactory *m_entityFactory; //!< The entity factory.
      EntityRepresentationFactory *m_representationFactory; //!< The representation factory.
  };

}

#endif
