/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Round.h"
#include <Config.h>
#include "ScoreCounter.h"
#include "LifeCounter.h"
#include "EntityRepresentationFactory.h"
#include "EntityFactory.h"
#include "Overlay.h"
#include "EntityRepresentationManager.h"
#include "EntityManager.h"
#include "Controllers/InvaderAI.h"
#include "Controllers/HumanTankController.h"
#include "EntityRepresentation.h"

#include "Entities/Bullet.h"

namespace SpaceInvaders {

  Round::Round(EntityFactory *entityFactory, EntityRepresentationFactory *representationFactory,
      InvaderController *invaderController, TankController *tankController,
      std::shared_ptr<ScoreCounter> &scoreCounter, std::shared_ptr<LifeCounter> &lifeCounter)
    : m_entityFactory(entityFactory), m_representationFactory(representationFactory),
      m_invaderController(invaderController), m_tankController(tankController),
      m_scoreCounter(scoreCounter), m_lifeCounter(lifeCounter)
  {
  }

  void Round::init()
  {
    // create overlay
    m_overlay = m_representationFactory->createOverlay(m_scoreCounter.get(), m_lifeCounter.get());
    m_overlay->init();

    // create the invaders
    m_invaderController->clearInvaders();
    int yOffset = INVADERS_Y_OFFSET;
    createRow(2, yOffset, 40);
    yOffset += 14 * PIXEL_SIZE;
    createRow(0, yOffset, 20);
    yOffset += 14 * PIXEL_SIZE;
    createRow(0, yOffset, 20);
    yOffset += 14 * PIXEL_SIZE;
    createRow(1, yOffset, 10);
    yOffset += 14 * PIXEL_SIZE;
    createRow(1, yOffset, 10);

    // create the tank
    std::shared_ptr<Entity> tank = m_entityFactory->createTank();
    tank->setPosition(20, 700);
    EntityManager::instance()->addEntity(tank);
    std::shared_ptr<EntityRepresentation> repr = m_representationFactory->createTankRepresentation(tank);
    tank->addObserver(repr);
    tank->addObserver(m_lifeCounter);
    EntityRepresentationManager::instance()->addRepresentation(repr);
    m_tankController->setTank(tank);


    // create barricades
    createBarricade(80, 600);
    createBarricade(336, 600);
    createBarricade(592, 600);
    createBarricade(848, 600);
  }

  void Round::update(sf::RenderWindow &window)
  {
    // clear window
    window.clear(sf::Color::Black);

    // draw all entity representations
    EntityRepresentationManager::instance()->draw(window);

    // draw overlay
    m_overlay->draw(window);

    // check for win/lost
    if (m_invaderController->invadersWon(650))
      m_lifeCounter->setLives(0);
    if (m_lifeCounter->lives() <= 0)
      return;

    // draw bounding boxes [debug]
    //drawBoundingBoxes(window);

    // get the elapsed time since last update
    sf::Time elapsed = m_clock.restart();

    // update all entities
    EntityManager::instance()->update(elapsed.asSeconds());

    // update controllers
    m_invaderController->update(elapsed.asSeconds());
    m_tankController->update(elapsed.asSeconds());

    // collision detection
    collisionDetection();

    // cleanup at end of frame
    EntityRepresentationManager::instance()->cleanup();
    EntityManager::instance()->cleanup();
  }

  void Round::createRow(int type, int yOffset, int score)
  {
    for (int i = 0; i < 11; ++i) {
      std::shared_ptr<Entity> invader = m_entityFactory->createInvader(score);
      // set the invader position
      invader->setPosition(INVADERS_X_OFFSET + i * (PIXEL_SIZE * 15), yOffset);
      // add the invader to the entity manager
      EntityManager::instance()->addEntity(invader);
      // add the invader to the invader controller
      m_invaderController->addInvader(invader);
      // create the invader representation
      std::shared_ptr<EntityRepresentation> repr = m_representationFactory->createInvaderRepresentation(invader, type);
      // add observers to invader
      invader->addObserver(repr);
      invader->addObserver(m_scoreCounter);
      // add representation to representation manager
      EntityRepresentationManager::instance()->addRepresentation(repr);
    }
  }

  void Round::createBarricadeBlock(int type, int x, int y, int offsetX, int offsetY)
  {
    std::shared_ptr<Entity> block = m_entityFactory->createBarricade();
    block->setPosition(x + offsetX, y + offsetY);
    EntityManager::instance()->addEntity(block);
    std::shared_ptr<EntityRepresentation> repr = m_representationFactory->createBarricadeRepresentation(block, type);
    block->addObserver(repr);
    EntityRepresentationManager::instance()->addRepresentation(repr);
  }

  void Round::createBarricade(int x, int y)
  {
    // type:         block:
    //
    // 2113          ABCD
    // 1451          EFGH
    // 1  1          I  J
    createBarricadeBlock(2, x, y,               0,               0); // A
    createBarricadeBlock(1, x, y,  6 * PIXEL_SIZE,               0); // B
    createBarricadeBlock(1, x, y, 12 * PIXEL_SIZE,               0); // C
    createBarricadeBlock(3, x, y, 18 * PIXEL_SIZE,               0); // D
    createBarricadeBlock(1, x, y,               0,  6 * PIXEL_SIZE); // E
    createBarricadeBlock(4, x, y,  6 * PIXEL_SIZE,  6 * PIXEL_SIZE); // F
    createBarricadeBlock(5, x, y, 12 * PIXEL_SIZE,  6 * PIXEL_SIZE); // G
    createBarricadeBlock(1, x, y, 18 * PIXEL_SIZE,  6 * PIXEL_SIZE); // H
    createBarricadeBlock(1, x, y,               0, 12 * PIXEL_SIZE); // I
    createBarricadeBlock(1, x, y, 18 * PIXEL_SIZE, 12 * PIXEL_SIZE); // J
  }

  void Round::drawBoundingBoxes(sf::RenderWindow &window)
  {
    sf::RectangleShape rect;
    rect.setFillColor(sf::Color(0, 0, 0, 0));
    rect.setOutlineThickness(1);
    rect.setOutlineColor(sf::Color(255, 0, 0));
    for (auto entity : EntityManager::instance()->entities()) {
      rect.setSize(entity->boundingBox());
      window.draw(rect, entity->transform());
    }
  }

  bool Round::collides(const sf::Vector2f &pos1, const sf::Vector2f &bb1,
      const sf::Vector2f &pos2, const sf::Vector2f &bb2)
  {
    if (pos1.x + 1 > pos2.x + bb2.x - 1)
      return false;
    if (pos2.x + 1 > pos1.x + bb1.x - 1)
      return false;
    if (pos1.y + 1 > pos2.y + bb2.y - 1)
      return false;
    if (pos2.y + 1 > pos1.y + bb1.y - 1)
      return false;
    return true;
  }

  void Round::collisionDetection()
  {
    for (auto entity : EntityManager::instance()->entities()) {
      // check if we have a bullet
      Bullet *bullet = dynamic_cast<Bullet*>(entity.get());
      if (!bullet)
        continue;

      // remove bullets that went offscreen
      if (bullet->position().y < -100 || bullet->position().y > SCREEN_HEIGHT + 100)
        bullet->removeLater();
    }

    // do collision detection
    auto entities = EntityManager::instance()->entities();
    for (std::size_t i = 0; i < entities.size(); ++i)
      for (std::size_t j = i + 1; j < entities.size(); ++j) {

        if (collides(entities[i]->position(), entities[i]->boundingBox(),
              entities[j]->position(), entities[j]->boundingBox())) {
          entities[i]->hit(entities[j]->damage());
          entities[j]->hit(entities[i]->damage());
        }
      }
  }

}
