/*
 * Copyright (c) 2013, Tim Vandermeersch
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SPACEINVADERS_ENTITYREPRESENTATION_H
#define SPACEINVADERS_ENTITYREPRESENTATION_H

#include "Observer.h"

#include <memory>

// forward declaration
namespace sf {
  class RenderWindow;
}

namespace SpaceInvaders {

  /**
   * @file EntityRepresentation.h
   */

  // forward declaration
  class Entity;

  /**
   * @brief Base class for entity representations.
   *
   * All graphical representations (i.e. invaders, bullets, ...) in the game
   * are derived from EntityRepresentation. In the Model-View-Controller
   * pattern, entities are the model. The views (i.e. EntityRepresentation) are
   * notified using the observer design pattern (i.e. Entity inherits Subject
   * and EntityRepresentation inherits Observer).
   */
  class EntityRepresentation : public Observer
  {
    public:
      /**
       * @brief Constructor.
       *
       * @param entity The entity.
       */
      EntityRepresentation(const std::shared_ptr<Entity> &entity);

      /**
       * @brief Destructor.
       */
      virtual ~EntityRepresentation();

      /**
       * @brief Get the associated entity.
       *
       * @return The associated entity.
       */
      Entity* entity() const
      {
        return m_entity.get();
      }

      /**
       * @brief Default implementation of the Observer::notify() function.
       *
       * This default implementation does nothing and is only provided
       * for entity representations that do not care about notifications.
       *
       * @param message The notification message.
       */
      void notify(int message = 0);

      /**
       * @brief Schedule this entity representation to be removed.
       *
       * The entity will be removed from the RepresentationManager when
       * RepresentationManager::cleanup() is called at the end of a frame.
       */
      void removeLater();

      /**
       * @brief Draw the representation of the entity.
       *
       * This function must be implemented by derived classes.
       *
       * @param window The SFML render window.
       */
      virtual void draw(sf::RenderWindow &window) = 0;

    private:
      std::shared_ptr<Entity> m_entity; //!< The associated entity.
  };

}

#endif
